# Ganda
Bachelorproef Aline Vercruysse

#### Front end 
Angular 5

#### Dabatase 
Firebase Realtime Database

#### Serverside functionaliteiten 
NodeJs, Firebase Cloud Functions

#### Styling
Sass

#### Concept

Ganda is een web applicatie waar gebruikers informatie kunnen krijgen over de werken in Gent. Ze kunnen deze informatie te zien krijgen door te zoeken naar werken of 
manifestaties in een radius rond een punt, of op een route. 
De gebruikers hebben ook de mogelijkheid om zich aan te melden en hun zoekopdrachten op te slaan en vervolgens via notificaties op de hoogte gehouden worden. 

## Live site

Surf naar 'https://ganda-app.firebaseapp.com'. Hier kan je een user account maken door te registreren en dan kan je gebruik maken van de applicatie.


 


