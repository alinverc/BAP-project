"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const functions = require("firebase-functions");
const admin = require("firebase-admin");
admin.initializeApp(functions.config().firebase);
const ref = admin.database().ref();
exports.hello = functions.https.onRequest((request, response) => {
    response.send("Hello from Firebase!");
});
exports.messageCron = functions.https.onRequest((request, response) => {
    const userId = 'qA7XdGzrF9eOyfWqneQLk1FcjGl1';
    const messages = admin.database().ref('/messages/' + userId + '/');
    const payload = {
        title: 'Zoek nu??',
        body: 'Ga naar je dashboard om te zoeken naar werken!',
        icon: "https://placekitten.com/g/200/300",
        click_action: "https://bap-testing.firebaseapp.com/dashboard"
    };
    const newRef = messages.push(payload);
    response.set('Notification sent');
});
exports.fcmSend = functions.database.ref('/messages/{userId}/{messageId}').onWrite(event => {
    const message = event.data.val();
    const userId = event.params.userId;
    const payload = {
        notification: {
            title: message.title,
            body: message.body,
            icon: "https://placekitten.com/g/200/300",
            click_action: "https://ganda-app.firebaseapp.com/dashboard"
        }
    };
    admin.database()
        .ref('/fcmTokens/' + userId)
        .once('value')
        .then(token => token.val())
        .then(userFcmToken => {
        return admin.messaging().sendToDevice(userFcmToken, payload);
    })
        .then(res => {
        console.log("Sent Successfully", res);
    })
        .catch(err => {
        console.log(err);
    });
});
//# sourceMappingURL=index.js.map