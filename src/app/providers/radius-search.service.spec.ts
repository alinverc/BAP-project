import { TestBed, inject } from '@angular/core/testing';

import { RadiusSearchService } from './radius-search.service';

describe('RadiusSearchService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [RadiusSearchService]
    });
  });

  it('should be created', inject([RadiusSearchService], (service: RadiusSearchService) => {
    expect(service).toBeTruthy();
  }));
});
