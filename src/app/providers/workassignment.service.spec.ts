import { TestBed, inject } from '@angular/core/testing';

import { WorkassignmentService } from './workassignment.service';

describe('WorkassignmentService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [WorkassignmentService]
    });
  });

  it('should be created', inject([WorkassignmentService], (service: WorkassignmentService) => {
    expect(service).toBeTruthy();
  }));
});
