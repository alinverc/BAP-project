import { Injectable } from '@angular/core';
import { Headers, Http, RequestOptions, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import { Workassignment } from '../models/workassignment';
import { WorkassignmentDetail } from '../models/workassignment-detail';

@Injectable()
export class WorkassignmentService {

  private apiUrl = 'https://api.gipod.vlaanderen.be/ws/v1/';

  constructor(public http: Http) {
    console.log('Hello WorkassignmentService Provider');
  }

  // Get all workassignments for the city Gent, max 1000 results
  getWorkassignments(): Observable<Workassignment[]> {
    const url = `${this.apiUrl}workassignment?city=Gent&limit=5000`;
    return this.http
        .get(url)
        .map((res: Response) => res.json())
        .catch((error: any) => Observable.throw(error.json().error || 'Server error'));
  }

  // Get all workassignments in a radius around a point
  GetWorkassignmentsRadius(pointA: number, pointB: number, radius: number): Observable<Workassignment[]> {
    const url = `${this.apiUrl}workassignment?point=${pointA},${pointB}&radius=${radius}&limit=5000&city=Gent`;
    return this.http
        .get(url)
        .map((res: Response) => res.json())
        .catch((error: any) => Observable.throw(error.json().error || 'Server error'));
  }

  // Get workassignment details by gipodId
  GetWorkassignmentDetail(id: number): Observable<WorkassignmentDetail[]> {
    const url = `${this.apiUrl}workassignment/${id}`;
    return this.http
        .get(url)
        .map((res: Response) => res.json())
        .catch((error: any) => Observable.throw(error.json().error || 'Server error'));
  }
}
