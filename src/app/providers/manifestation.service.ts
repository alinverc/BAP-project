import { Injectable } from '@angular/core';
import { Headers, Http, RequestOptions, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import { Manifestation } from '../models/manifestation';
import { ManifestationDetail } from '../models/manifestation-detail';

@Injectable()
export class ManifestationService {

  private apiUrl = 'https://api.gipod.vlaanderen.be/ws/v1/';

  constructor(public http: Http) {
    console.log('Hello ManifestationService Provider');
  }

  // Get all manifestations for the city Gent, max 100 results
  getmanifestations(): Observable<Manifestation[]> {
    // adding options -> application/json breaks it, issues with access allow control origin stuff
    const url = `${this.apiUrl}manifestation?city=Gent&limit=5000`;
    return this.http
        .get(url)
        .map((res: Response) => res.json())
        .catch((error: any) => Observable.throw(error.json().error || 'Server error'));
  }

  // Get all manifestations in a radius around a point
  GetManifestationsRadius(pointA: number, pointB: number, radius: number): Observable<Manifestation[]> {
    const url = `${this.apiUrl}manifestation?point=${pointA},${pointB}&radius=${radius}&limit=5000&city=Gent`;
    return this.http
        .get(url)
        .map((res: Response) => res.json())
        .catch((error: any) => Observable.throw(error.json().error || 'Server error'));
  }

  // Get manifestation details by gipodId
  GetManifestationDetail(id: number): Observable<ManifestationDetail[]> {
    const url = `${this.apiUrl}manifestation/${id}`;
    return this.http
        .get(url)
        .map((res: Response) => res.json())
        .catch((error: any) => Observable.throw(error.json().error || 'Server error'));
  }

}
