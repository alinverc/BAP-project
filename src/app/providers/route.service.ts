import { Injectable } from '@angular/core';
import { AngularFireDatabase, FirebaseListObservable } from 'angularfire2/database-deprecated';
import { AngularFireAuth } from 'angularfire2/auth';
import { Route } from '../models/route';

@Injectable()
export class RouteService {
  routes: FirebaseListObservable<Route[]> = null;
  userId: string;
  route: FirebaseListObservable<Route[]>;

  constructor(private db: AngularFireDatabase, private afAuth: AngularFireAuth) {
    this.afAuth.authState.subscribe(user => {
      if (user) {
        this.userId = user.uid;
      }
    });
  }

  // get routes for currently logged in user
  getRoutesList(userId): FirebaseListObservable<Route[]> {
    if (!userId) {
      console.log('no user, routeService');
      return;
    }
    this.routes = this.db.list(`routes/`, {
        query: {
            orderByChild: 'userId',
            equalTo: userId // currentUser.uid
        }
    });
    return this.routes;
  }

  // get route by name
  getRouteByName(name): FirebaseListObservable<Route[]> {
    this.route = this.db.list(`routes/`, {
        query: {
            orderByChild: 'name',
            equalTo: name
        }
    });
    return this.route;
  }

  // get routes for currently logged in user
  getAllRoutesList(): FirebaseListObservable<Route[]> {
    this.routes = this.db.list(`routes/`, {
        query: {
            orderByChild: 'userId',
        }
    });
    return this.routes;
  }
}
