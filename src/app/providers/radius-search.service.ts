import { Injectable } from '@angular/core';
declare var google: any;
import { Observable } from 'rxjs/Observable';

@Injectable()
export class RadiusSearchService {

  constructor() { }

  getCoordsDescs(self, radius, type): any {
    let i = 0;
      for (i = 0; i < radius.length; i++) {
        if (radius[i].importantHindrance === false) {
          console.log('hindrance works', radius[i].importantHindrance);
        } else {
        self.markers.push({
          lat: radius[i].coordinate.coordinates[1],
          lng: radius[i].coordinate.coordinates[0],
          draggable: false
        });
        self.info.push({
          desc: radius[i].description,
          gipodId: radius[i].gipodId,
          type: type
        });
      }
    }
    console.log('get coordsdescs', self.markers, self.info);
    return{};
  }

  geocode(self, point): any {
    let geocoder;
    geocoder = new google.maps.Geocoder();
    geocoder.geocode( { 'address': point}, function(results, status) {
      self.adressInput = results[0].geometry.location;
      if (status === 'OK') {
        console.log('geocode OK');
      } else {
        console.log('Er is iets misgegaan, probeer opnieuw');
        // location.reload();
      }
      // check if address is in Gent
      // if (results[0].address_components[1].long_name !== 'Gent') {
      //   console.log('geocode not gent', results);
      //   // alert('Dit adres ligt niet in Gent');
      // }
    });
    return{};
  }

}
