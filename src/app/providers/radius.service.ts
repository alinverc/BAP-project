import { Injectable } from '@angular/core';
import { AngularFireDatabase, FirebaseListObservable } from 'angularfire2/database-deprecated';
import { AngularFireAuth } from 'angularfire2/auth';
import { Radius } from '../models/radius';

@Injectable()
export class RadiusService {
  roadii: FirebaseListObservable<Radius[]> = null;
  userId: string;
  radius: FirebaseListObservable<Radius[]>;

  constructor(private db: AngularFireDatabase, private afAuth: AngularFireAuth) {
    this.afAuth.authState.subscribe(user => {
      if (user) {
        this.userId = user.uid;
      }
    });
  }

  // get radius for currently logged in user
  getRadiiList(): FirebaseListObservable<Radius[]> {
    if (!this.userId) {
      console.log('no user, radiusService');
      return;
    }
    this.roadii = this.db.list(`radius/`, {
        query: {
            orderByChild: 'userId',
            equalTo: this.userId
        }
    });
    return this.roadii;
  }

  // get radius by name
  getRadiusByName(name): FirebaseListObservable<Radius[]> {
    this.radius = this.db.list(`radius/`, {
        query: {
            orderByChild: 'name',
            equalTo: name
        }
    });
    return this.radius;
  }

}
