import { Injectable } from '@angular/core';
declare var google: any;
import { Observable } from 'rxjs/Observable';
import { styles } from '../styles/map';

@Injectable()
export class RouteSearchService {

  constructor() { }

    // show route, make polyline
    showRoute(self, origin, destination, travelMode): any {
      // show map
      let directionsDisplay;
      const mapOptions = {
        zoom: 11,
        center: origin,
        styles: styles,
        zoomControl: false,
        mapTypeControl: false,
        scaleControl: false,
        streetViewControl: false,
        fullscreenControl: false,
      };
      self.newMap = new google.maps.Map(document.getElementById('map'), mapOptions);
      const directionsService = new google.maps.DirectionsService();
      directionsDisplay = new google.maps.DirectionsRenderer({ polylineOptions: {
          strokeColor: '#6CC0CA',
          strokeWeight: '5'
        } });
      // display on map
      directionsDisplay.setMap(self.newMap);
      const request = {
        origin: origin,
        destination: destination,
        travelMode: travelMode
      };
      directionsService.route(request, function(response, status) {
        if (status === 'OK') {
          directionsDisplay.setDirections(response);
        } else {
          console.log(status, response);
        }
        console.log('DEBUGGING', directionsDisplay);
        // get polyline overview and decode
        self.overviewPolyline = directionsDisplay.directions.routes[0].overview_polyline;
        self.decodedPolyline = google.maps.geometry.encoding.decodePath
          (self.overviewPolyline);

        // make polyline from decoded polyline info
        self.newPolyline = new google.maps.Polyline({
          path: self.decodedPolyline
        });
        console.log('dis?', self.newPolyline);
      });
      return {};
    }

    searchOnPolyline(self, latLng, positions): any {
      let onRoute: boolean;
      console.log('latlng', latLng);
      let i = 0;
      for (i = 0; i < latLng.length; i++) {
        if (google.maps.geometry.poly.isLocationOnEdge(positions[i], self.newPolyline, 0.001)) {
          console.log('yup');
          onRoute = true;
          if (onRoute === true) {
            const marker = new google.maps.Marker({
              position: positions[i],
              animation: google.maps.Animation.BOUNCE,
              map: self.newMap,
            });
            self.info.push({
              desc: latLng[i].desc,
              gipodId: latLng[i].gipodId,
              type: latLng[i].type
            });
          }
        } else {
          console.log('njo');
        }
      }
      return {};
    }
}
