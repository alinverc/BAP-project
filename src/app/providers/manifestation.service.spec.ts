import { TestBed, inject } from '@angular/core/testing';

import { ManifestationService } from './manifestation.service';

describe('ManifestationService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ManifestationService]
    });
  });

  it('should be created', inject([ManifestationService], (service: ManifestationService) => {
    expect(service).toBeTruthy();
  }));
});
