import { Component, OnInit } from '@angular/core';
declare var google: any;
import { Manifestation } from '../models/manifestation';
import { Workassignment } from '../models/workassignment';
import { ManifestationService } from '../providers/manifestation.service';
import { WorkassignmentService } from '../providers/workassignment.service';
import { RouteSearchService } from '../providers/route-search.service';
import { empty } from 'rxjs/Observer';
import { NavigationComponent } from '../core/navigation/navigation.component';
import { AngularFireModule } from 'angularfire2';
import { AngularFireDatabase, FirebaseListObservable } from 'angularfire2/database-deprecated';
import { AngularFireAuth } from 'angularfire2/auth';
import { Observable } from 'rxjs/Observable';
import * as firebase from 'firebase/app';
import { AuthService } from '../core/auth.service';
import { FirebaseApp } from 'angularfire2/firebase.app.module';
import { Router } from '@angular/router';
import { styles } from '../styles/map';

@Component({
  selector: 'app-route-search',
  templateUrl: './route-search.component.html',
  styleUrls: ['./route-search.component.scss'],
  providers: [ManifestationService, WorkassignmentService,
    AuthService, RouteSearchService]
})
export class RouteSearchComponent implements OnInit {
  items: FirebaseListObservable<Item[]>;
  userId: string;
  autocomplete;
  otherAutocomplete;
  map: any;
  newMap: any;
  overviewPolyline: any;
  decodedPolyline: any;
  newPolyline: any;
  inputOrigin: any;
  inputDestination: any;
  private manifestations: Manifestation[] = [];
  private workassignments: Workassignment[] = [];
  private error: string;
  info = [];
  latLng = [];
  positions = [];
  location = {lat: 51.0543, lng: 3.7174};
  autocmpltOrigin;
  autocmpltDest;
  selectedTravelMode;

  travelModes = [
    {value: 'DRIVING', name: 'Auto'},
    {value: 'BICYCLING', name: 'Fiets'},
    {value: 'WALKING', name: 'Te voet'}
  ];

  constructor(private manifestationService: ManifestationService,
    private workassignmentService: WorkassignmentService,
    private routeSearchService: RouteSearchService,
    public authService: AuthService,
    public afAuth: AngularFireAuth, public af: AngularFireDatabase,
    private router: Router) {
      this.afAuth.authState.subscribe(user => {
        if (user) {
          this.userId = user.uid;
        }
      });
      this.items = af.list('/routes', {
        query: {
          limitToLast: 500
        }
      });
    }

  ngOnInit() {
    console.log('init', this.items);
      // render map
      this.map = new google.maps.Map(document.getElementById('map'), {
        center: this.location,
        zoom: 13,
        zoomControl: false,
        mapTypeControl: false,
        scaleControl: false,
        streetViewControl: false,
        fullscreenControl: false,
        styles: styles
      });
      const options = {
        bounds: this.map,
        types: ['(cities)'],
      };
      const inputO = document.getElementById('originInput');
      const inputD = document.getElementById('destinationInput');
      this.autocomplete = new google.maps.places.Autocomplete(inputO);
      this.otherAutocomplete = new google.maps.places.Autocomplete(inputD);
      this.autocomplete.bindTo('bounds', this. map);
      this.otherAutocomplete.bindTo('bounds', this. map);

      // get manifestations in Gent
      this.manifestationService
      .getmanifestations()
      .subscribe((manifestations: Manifestation[]) => {
        this.manifestations = manifestations;
        // get coordinates from each manifestation with important hindrance, put in array
        const coordinates = this;
        let i = 0;
        for (i = 0; i < this.manifestations.length; i++) {
          if (this.manifestations[i].importantHindrance === false) {
            console.log('hindrance manifs', this.manifestations[i].importantHindrance);
           } else {
            coordinates.latLng.push({
              lat: this.manifestations[i].coordinate.coordinates[1],
              lng: this.manifestations[i].coordinate.coordinates[0],
              desc: this.manifestations[i].description,
              gipodId: this.manifestations[i].gipodId,
              type: 'manifestation'
             });
           }
        }
      });

      // get workassignments in Gent
      this.workassignmentService
      .getWorkassignments()
      .subscribe((workassignments: Workassignment[]) => {
        this.workassignments = workassignments;
        // get coordinates from each work with important hindrance, put in array
        const coordinates = this;
        let i = 0;
        for (i = 0; i < this.workassignments.length; i++) {
          if (this.workassignments[i].importantHindrance === false) {
            console.log('hindrance works', this.workassignments[i].importantHindrance);
           } else {
            coordinates.latLng.push({
              lat: this.workassignments[i].coordinate.coordinates[1],
              lng: this.workassignments[i].coordinate.coordinates[0],
              desc: this.workassignments[i].description,
              gipodId: this.workassignments[i].gipodId,
              type: 'workassignment'
             });
           }
        }
      });
    console.log('latLng', this.latLng);
  }

  // get manifestation and work latlng and make google.latlng
  getPositions() {
      let i = 0;
      for (i = 0; i < this.latLng.length; i++) {
        const googleThingie = new google.maps.LatLng(this.latLng[i].lat,
          this.latLng[i].lng);
        this.positions.push(googleThingie);
      }
      console.log('positions', this.positions);
  }

  // take address and geocode to get Lat and Lng
  geocode(point, bool) {
    let geocoder;
    const self = this;
    geocoder = new google.maps.Geocoder();
    geocoder.geocode( { 'address': point}, function(results, status) {
      if (bool === true) {
        self.inputOrigin = results[0].geometry.location;
      } else if (bool === false) {
        self.inputDestination = results[0].geometry.location;
      }
      if (status === 'OK') {
        console.log('geocode OK');
      } else {
        console.log('Er is iets misgegaan, probeer opnieuw');
        location.reload();
      }
    });
  }

  routeSearch(origin, destination, selectedValue) {
    if (origin === undefined || destination === undefined || selectedValue === undefined) {
      alert('Gelieve alle zoekvelden in te vullen');
      return;
    } else if ( origin === '' || destination === '' || selectedValue === '' ) {
      alert('Gelieve alle zoekvelden in te vullen');
      return;
    }
    // get autocompleted user input for geocoding
    const place = this.autocomplete.getPlace();
    this.autocmpltOrigin = place.formatted_address;
    const placeD = this.otherAutocomplete.getPlace();
    this.autocmpltDest = placeD.formatted_address;
    this.selectedTravelMode = selectedValue;
    // make sure everything from last search is gone
    this.emptyArrays();

    // geocode inputs
    this.geocode(this.autocmpltOrigin, true);
    this.geocode(this.autocmpltDest, false);
    setTimeout(() => {
      console.log('origin', this.autocmpltOrigin);
      console.log('destination', this.autocmpltDest);
    }, 100);

    // show map with route
    setTimeout(() => {
      const self = this;
      this.routeSearchService.showRoute(self, this.inputOrigin, this.inputDestination, selectedValue);
    }, 100);

    // get Lat and Lng of manifestations to make positions to check with
     this.getPositions();

    // Check if positions are on polyline, if yes, display marker
    setTimeout(() => {
      const self = this;
      console.log('new polyline', this.newPolyline, this.map);
      this.routeSearchService.searchOnPolyline(self, this.latLng, this.positions);
    }, 500);
  }

  // Emtpy stuff for new search
  emptyArrays() {
    this.info.length = 0;
    this.overviewPolyline = '';
    this.decodedPolyline = '';
    this.newPolyline = '';
    this.inputOrigin = '';
    this.inputDestination = '';
  }

  saveRoute() {
    if (this.autocmpltDest === undefined || this.autocmpltOrigin || this.selectedTravelMode === undefined) {
      if (confirm('Opgelet! Gelieve alle zoekvelden in te vullen')) {
      }
    } else {
      this.afAuth.authState.subscribe(res => {
        if (res && res.uid) {
          this.router.navigate(['/zoeken/route/new/'
          + this.autocmpltOrigin + '/' + this.autocmpltDest + '/' + this.selectedTravelMode]);
        } else {
          if (confirm('Gelieve eerst in te loggen of te registreren')) {
            this.router.navigate(['']);
          } else {
              // Do nothing!
          }
        }
      });
    }

  }

}

export class Item {
  origin: any;
  destination: any;
  userId: string;
}
