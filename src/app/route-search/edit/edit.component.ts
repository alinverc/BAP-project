import { Component, OnInit } from '@angular/core';
import { AngularFireModule } from 'angularfire2';
import { AngularFireDatabase, FirebaseListObservable } from 'angularfire2/database-deprecated';
import { Observable } from 'rxjs/Observable';
import * as firebase from 'firebase';
import { RouteService } from '../../providers/route.service';
import { FormsModule, ReactiveFormsModule, FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Route } from '../../models/route';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.scss'],
  providers: [RouteService]
})
export class EditComponent implements OnInit {
  private routeName: any;
  private route: FirebaseListObservable<Route[]>;
  private sub: any;

  constructor(public routeService: RouteService,
    private activeRoute: ActivatedRoute,
    private router: Router) { }

  ngOnInit() {
    this.sub = this.activeRoute.params.subscribe(params => {
      this.routeName = params['name'];
    });
    this.route = this.routeService.getRouteByName(this.routeName);
  }

  updateRoute(key: string, newName: string, desc: string) {
    this.route.update(key, { name: newName, desc: desc });
    this.router.navigate(['/dashboard']);
  }

}
