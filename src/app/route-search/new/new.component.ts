import { Component, OnInit } from '@angular/core';
import { AngularFireModule } from 'angularfire2';
import { AngularFireDatabase, FirebaseListObservable } from 'angularfire2/database-deprecated';
import { AngularFireAuth } from 'angularfire2/auth';
import { Observable } from 'rxjs/Observable';
import * as firebase from 'firebase/app';
import { AuthService } from '../../core/auth.service';
import { FirebaseApp } from 'angularfire2/firebase.app.module';
import { Route } from '../../models/route';
import { ActivatedRoute } from '@angular/router';
import { Router } from '@angular/router';

@Component({
  selector: 'app-new',
  templateUrl: './new.component.html',
  styleUrls: ['./new.component.scss'],
  providers: [AuthService]
})
export class NewComponent implements OnInit {
  private sub: any;
  route: FirebaseListObservable<Route[]>;
  destination: string;
  travelMode: any;
  origin: string;
  name: string;
  desc: string;
  userId: string;

  constructor(public authService: AuthService,
    public afAuth: AngularFireAuth,
    public af: AngularFireDatabase,
    private firebaseAuth: AngularFireAuth,
    private db: AngularFireDatabase,
    private activeRoute: ActivatedRoute,
    private router: Router) {
      this.afAuth.authState.subscribe(user => {
        if (user) {
          this.userId = user.uid;
        }
      });
      this.route = af.list('/routes');
  }

  ngOnInit() {
    this.sub = this.activeRoute.params.subscribe(params => {
      this.origin = params['autocmpltOrigin'];
      this.destination = params['autocmpltDest'];
      this.travelMode = params['selectedTravelMode'];
    });
    console.log(this.origin, this.destination, this.travelMode);
  }

  // save route to database
  save(name, desc) {
    this.route.push({ origin: this.origin, destination: this.destination, travelMode: this.travelMode,
      userId: this.userId, name: name, desc: desc});
      this.router.navigate(['/dashboard']);
  }

}
