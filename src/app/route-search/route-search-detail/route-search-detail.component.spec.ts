import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RouteSearchDetailComponent } from './route-search-detail.component';

describe('RouteSearchDetailComponent', () => {
  let component: RouteSearchDetailComponent;
  let fixture: ComponentFixture<RouteSearchDetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RouteSearchDetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RouteSearchDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
