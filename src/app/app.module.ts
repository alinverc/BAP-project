import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpModule } from '@angular/http';
import { FormsModule } from '@angular/forms';


import { AngularFireModule } from 'angularfire2';
import { AngularFireDatabaseModule } from 'angularfire2/database-deprecated';
import { AngularFireAuthModule } from 'angularfire2/auth';

import { AppRoutingModule } from './app-routing.module';

import { AppComponent } from './app.component';
import { LandingpageComponent } from './landingpage/landingpage.component';
import { NavigationComponent } from './core/navigation/navigation.component';
import { SearchComponent } from './search/search.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { RadiusSearchComponent } from './radius-search/radius-search.component';

import { ManifestationService } from './providers/manifestation.service';
import { RadiusSearchDetailComponent } from './radius-search/radius-search-detail/radius-search-detail.component';
import { RouteSearchComponent } from './route-search/route-search.component';
import { RouteSearchDetailComponent } from './route-search/route-search-detail/route-search-detail.component';
import { NewComponent } from './route-search/new/new.component';
import { EditComponent } from './route-search/edit/edit.component';
import { NewRadiusComponent } from './radius-search/new-radius/new-radius.component';
import { RadiusEditComponent } from './radius-search/radius-edit/radius-edit.component';
import { MasterDashboardComponent } from './dashboard/master-dashboard/master-dashboard.component';

export const firebaseConfig = {
  apiKey: 'AIzaSyD3OKxVjKTA4uHIgZme6ByrFykzSUl-BKk',
  authDomain: 'ganda-app.firebaseapp.com',
  databaseURL: 'https://ganda-app.firebaseio.com',
  projectId: 'ganda-app',
  storageBucket: 'ganda-app.appspot.com',
  messagingSenderId: '674999210384'
};

@NgModule({
  declarations: [
    AppComponent,
    LandingpageComponent,
    NavigationComponent,
    SearchComponent,
    DashboardComponent,
    RadiusSearchComponent,
    RadiusSearchDetailComponent,
    RouteSearchComponent,
    RouteSearchDetailComponent,
    NewComponent,
    EditComponent,
    NewRadiusComponent,
    RadiusEditComponent,
    MasterDashboardComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpModule,
    FormsModule,
    AngularFireModule.initializeApp(firebaseConfig),
    AngularFireDatabaseModule,
    AngularFireAuthModule
  ],
  providers: [
    ManifestationService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
