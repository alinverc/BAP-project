import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RadiusSearchDetailComponent } from './radius-search-detail.component';

describe('RadiusSearchDetailComponent', () => {
  let component: RadiusSearchDetailComponent;
  let fixture: ComponentFixture<RadiusSearchDetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RadiusSearchDetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RadiusSearchDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
