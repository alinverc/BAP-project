import { Component, OnInit } from '@angular/core';
declare var google: any;
import { ActivatedRoute } from '@angular/router';
import { DatePipe } from '@angular/common';
import { ManifestationDetail } from '../../models/manifestation-detail';
import { ManifestationService } from '../../providers/manifestation.service';
import { WorkassignmentDetail } from '../../models/workassignment-detail';
import { WorkassignmentService } from '../../providers/workassignment.service';
import { Detail } from '../../models/detail';
import { NavigationComponent } from '../../core/navigation/navigation.component';
import { styles } from '../../styles/map';

@Component({
  selector: 'app-radius-search-detail',
  templateUrl: './radius-search-detail.component.html',
  styleUrls: ['./radius-search-detail.component.scss'],
  providers: [ManifestationService, WorkassignmentService]
})
export class RadiusSearchDetailComponent implements OnInit {
  private gipodId: number;
  private type: string;
  private sub: any;
  private manifestation: ManifestationDetail[];
  private workassignment: WorkassignmentDetail[];
  marker = [];
  detail: any;
  map: any;

  constructor(
    private route: ActivatedRoute,
    private manifestationService: ManifestationService,
    private workassignmentService: WorkassignmentService) { }

  ngOnInit() {

    const location = {lat: 51.0543, lng: 3.7174};

    // get ID
    this.sub = this.route.params.subscribe(params => {
      this.gipodId = +params['gipodId'];
      this.type = params['type'];
    });
    if (this.type === 'manifestation') {
      // get manifestation details by ID
      this.manifestationService.GetManifestationDetail(this.gipodId)
      .subscribe((manifestation: ManifestationDetail[]) => {
        this.detail = manifestation;
        console.log(this.detail, 'manifestation details');
        // make marker with coordinates
        this.marker = new google.maps.LatLng(this.detail.location.coordinate.coordinates[1],
          this.detail.location.coordinate.coordinates[0]);
        console.log(this.marker, 'marker');
        this.map = new google.maps.Map(document.getElementById('map'), {
          center: this.marker,
          zoom: 15
        });
        const marker = new google.maps.Marker({
          position: this.marker,
          animation: google.maps.Animation.BOUNCE, // BOUNCE
          map: this.map,
        });
      });

    } else if (this.type === 'workassignment') {
      // get workassignemnt details by ID
      this.workassignmentService.GetWorkassignmentDetail(this.gipodId)
      .subscribe((workassignment: WorkassignmentDetail[]) => {
        this.detail = workassignment;
        console.log(this.detail, 'workassignment details');
        // make marker with coordinates, init map, show marker
        this.marker = new google.maps.LatLng(this.detail.location.coordinate.coordinates[1],
          this.detail.location.coordinate.coordinates[0]);
        console.log(this.marker, 'marker');
        this.map = new google.maps.Map(document.getElementById('map'), {
          center: this.marker,
          zoom: 15,
          styles: styles,
          zoomControl: false,
          mapTypeControl: false,
          scaleControl: false,
          streetViewControl: false,
          fullscreenControl: false,
        });
        const marker = new google.maps.Marker({
          position: this.marker,
          animation: google.maps.Animation.BOUNCE, // BOUNCE
          map: this.map,
        });
      });
    }

  }

}
