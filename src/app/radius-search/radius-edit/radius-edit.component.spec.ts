import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RadiusEditComponent } from './radius-edit.component';

describe('RadiusEditComponent', () => {
  let component: RadiusEditComponent;
  let fixture: ComponentFixture<RadiusEditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RadiusEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RadiusEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
