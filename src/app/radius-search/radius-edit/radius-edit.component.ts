import { Component, OnInit } from '@angular/core';
import { AngularFireModule } from 'angularfire2';
import { AngularFireDatabase, FirebaseListObservable } from 'angularfire2/database-deprecated';
import { Observable } from 'rxjs/Observable';
import * as firebase from 'firebase/app';
import { RadiusService } from '../../providers/radius.service';
import { FormsModule, ReactiveFormsModule, FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Radius } from '../../models/radius';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-edit',
  templateUrl: './radius-edit.component.html',
  styleUrls: ['./radius-edit.component.scss'],
  providers: [RadiusService]
})
export class RadiusEditComponent implements OnInit {
  private radiusName: any;
  private radius: FirebaseListObservable<Radius[]>;
  private sub: any;

  constructor(public radiusService: RadiusService,
    private activeRoute: ActivatedRoute,
    private router: Router) { }

  ngOnInit() {
    this.sub = this.activeRoute.params.subscribe(params => {
      this.radiusName = params['name'];
    });
    this.radius = this.radiusService.getRadiusByName(this.radiusName);
  }

  updateRadius(key: string, newName: string, desc: string) {
    this.radius.update(key, { name: newName, desc: desc });
    this.router.navigate(['/dashboard']);
  }

}
