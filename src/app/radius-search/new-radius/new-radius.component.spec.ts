import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NewRadiusComponent } from './new-radius.component';

describe('NewRadiusComponent', () => {
  let component: NewRadiusComponent;
  let fixture: ComponentFixture<NewRadiusComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NewRadiusComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NewRadiusComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
