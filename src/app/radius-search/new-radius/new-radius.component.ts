import { Component, OnInit } from '@angular/core';
import { AngularFireModule } from 'angularfire2';
import { AngularFireDatabase, FirebaseListObservable } from 'angularfire2/database-deprecated';
import { AngularFireAuth } from 'angularfire2/auth';
import { Observable } from 'rxjs/Observable';
import * as firebase from 'firebase/app';
import { AuthService } from '../../core/auth.service';
import { FirebaseApp } from 'angularfire2/firebase.app.module';
import { Radius } from '../../models/radius';
import { ActivatedRoute } from '@angular/router';
import { Router } from '@angular/router';

@Component({
  selector: 'app-new-radius',
  templateUrl: './new-radius.component.html',
  styleUrls: ['./new-radius.component.scss'],
  providers: [AuthService]
})
export class NewRadiusComponent implements OnInit {
  private sub: any;
  radius: FirebaseListObservable<Radius[]>;
  address;
  radiusInput: string;
  name: string;
  desc: string;
  userId: string;
  markers;

  constructor(public authService: AuthService,
    public afAuth: AngularFireAuth,
    public af: AngularFireDatabase,
    private firebaseAuth: AngularFireAuth,
    private db: AngularFireDatabase,
    private activeRoute: ActivatedRoute,
    private router: Router) {
      this.afAuth.authState.subscribe(user => {
        if (user) {
          this.userId = user.uid;
        }
      });
      this.radius = af.list('/radius');
     }

  ngOnInit() {
    this.sub = this.activeRoute.params.subscribe(params => {
      this.address = params['autocmpltInput'];
      this.radiusInput = params['radius'];
      this.markers = params['markers'];
    });
    console.log(this.address, this.radiusInput, this.markers);
  }

  // save radius to database
  save(name, desc) {
    this.radius.push({ address: this.address, radius: this.radiusInput,
      userId: this.userId, name: name, desc: desc});
      this.router.navigate(['/dashboard']);
  }

}
