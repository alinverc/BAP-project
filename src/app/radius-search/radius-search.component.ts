import { Component, OnInit } from '@angular/core';
declare var google: any;
import { Manifestation } from '../models/manifestation';
import { Workassignment } from '../models/workassignment';
import { ManifestationService } from '../providers/manifestation.service';
import { WorkassignmentService } from '../providers/workassignment.service';
import { RadiusSearchService } from '../providers/radius-search.service';
import { NavigationComponent } from '../core/navigation/navigation.component';
import { AuthService } from '../core/auth.service';
import { AngularFireAuth } from 'angularfire2/auth';
import { Router } from '@angular/router';
import { styles } from '../styles/map';

@Component({
  selector: 'app-radius-search',
  templateUrl: './radius-search.component.html',
  styleUrls: ['./radius-search.component.scss'],
  providers: [ManifestationService, WorkassignmentService, RadiusSearchService]
})
export class RadiusSearchComponent implements OnInit {
  map: any;
  autocomplete;
  private error: string;
  private manifestations: Manifestation[] = [];
  private workassignments: Workassignment[] = [];
  private manifestationsRadius: Manifestation[] = [];
  private workassignmentRadius: Workassignment[] = [];
  markers: Marker[] = [];
  manifestationDescs = [];
  info = [];
  adressInput: any;
  disableBtn: boolean;
  autocmpltInput;
  radius;

  constructor(private manifestationService: ManifestationService,
    private workassignmentService: WorkassignmentService,
    private radiusService: RadiusSearchService,
    public authService: AuthService,
    public afAuth: AngularFireAuth,
    private router: Router) { }

  ngOnInit() {
    const location = {lat: 51.0543, lng: 3.7174};
      // render map
      this.map = new google.maps.Map(document.getElementById('map'), {
        center: location,
        zoom: 13,
        zoomControl: false,
        mapTypeControl: false,
        scaleControl: false,
        streetViewControl: false,
        fullscreenControl: false,
        styles: styles
      });

      const input = document.getElementById('input');
      this.autocomplete = new google.maps.places.Autocomplete(input);
      this.autocomplete.bindTo('bounds', this. map);
  }
  // SEARCH
  radiusSearch(radius, street: string) {
    this.radius = radius;
    this.emptyArrays();
    // VALIDATION
    if (street === undefined || radius === undefined) {
      alert('Gelieve een adres en een radius in te vullen');
      return;
    }  else if (street === '' || radius === '') {
      alert('Gelieve een adres en een radius in te vullen');
      return;
    } else if (isNaN(radius)) {
      alert('Gelieve een geldige radius in te vullen, bijvoorbeeld 500');
      return;
    }
    const place = this.autocomplete.getPlace();
    this.autocmpltInput = place.formatted_address;
    // get address from input and geocode to get coordinates from adress
    this.radiusService.geocode(this, this.autocmpltInput);

    setTimeout(() => {
      console.log('addresinput', this.adressInput);
      const lat = this.adressInput.lat();
      const lng = this.adressInput.lng();

      // WORKASSIGNMENTS
      this.workassignmentService.
      GetWorkassignmentsRadius(lng, lat, radius)
      .subscribe((workassignments: Workassignment[]) => {
        console.log(workassignments, 'radius search workassignments');
        this.workassignmentRadius = workassignments;
        // check if workassignments are found
        if (workassignments.length === 0) {
          return;
        }
        // get coordinates and descriptions of found workassignments
        this.radiusService.getCoordsDescs(this, this.workassignmentRadius, 'workassignment');
      }); // works stuff

      // MANIFESTATIONS
      this.manifestationService
      .GetManifestationsRadius(lng, lat, radius)
      .subscribe((manifestations: Manifestation[]) => {
        console.log(manifestations, 'radius search manifestations');
        this.manifestationsRadius = manifestations;
        // check if any manifestations are found
        if (manifestations.length === 0) {
          this.error = 'Er zijn geen werken gevonden!';
        }
        // get coordinates and descriptions of found manifestations
        this.radiusService.getCoordsDescs(this, this.manifestationsRadius, 'manifestation');
      });
      console.log(this.info, 'descs');
    }, 500);

    // display markers
    setTimeout(() => {
      console.log(this.markers, 'markers');
      this.map = new google.maps.Map(document.getElementById('map'), {
        center: this.adressInput,
        zoom: 13,
        styles: styles,
        zoomControl: false,
        mapTypeControl: false,
        scaleControl: false,
        streetViewControl: false,
        fullscreenControl: false,
      });
      let i = 0;
      for (i = 0; i < this.markers.length; i++) {
        const markers = new google.maps.Marker({
          position: this.markers[i],
          animation: google.maps.Animation.DROP,
          map: this.map,
        });
      }
      const radiusCircle = new google.maps.Circle({
        strokeColor: '#5FA385',
        strokeOpacity: 0.8,
        strokeWeight: 2,
        fillColor: '#A6D395',
        fillOpacity: 0.35,
        map: this.map,
        center: this.adressInput,
        radius: +radius
      });
      const marker = new google.maps.Marker({
        position: this.adressInput,
        animation: google.maps.Animation.BOUNCE,
        map: this.map,
      });
    }, 1000);
    // disable button for 1 sec to prevent errors
    this.disable();
    setTimeout(() => {
      this.disable();
    }, 1000);
  }

  emptyArrays() {
    this.manifestationsRadius.length = 0;
    this.manifestationDescs.length = 0;
    this.info.length = 0;
    this.markers.length = 0;
    this.error = '';
  }

  disable() {
    this.disableBtn = !this.disableBtn;
  }

  checkUser() {
    console.log('user is logged in!');
    // check if user is logged in or not
    this.afAuth.authState.subscribe(res => {
      if (res && res.uid) {
        console.log('user is logged in!');
      }
    });
  }
  saveRadius() {
    if (this.autocmpltInput === undefined || this.radius === undefined) {
      alert('Opgelet! Gelieve alle zoekvelden in te vullen');
      return;
    } else if (this.autocmpltInput === '' || this.radius === '') {
      alert('Opgelet! Gelieve alle zoekvelden in te vullen');
      return;
    } else {
      this.afAuth.authState.subscribe(res => {
        if (res && res.uid) {
          this.router.navigate(['/zoeken/radius/new/' + this.autocmpltInput + '/' + this.radius]);
        } else {
            if (confirm('Gelieve eerst in te loggen of te registreren')) {
              this.router.navigate(['']);
          } else {
              // Do nothing!
          }
        }
      });
    }
  }
}



export class Marker {
  lat?: {};
  lng?: {};
  draggable?: false;
}
