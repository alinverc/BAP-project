import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AppComponent } from './app.component';
import { LandingpageComponent } from './landingpage/landingpage.component';
import { SearchComponent } from './search/search.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { MasterDashboardComponent } from './dashboard/master-dashboard/master-dashboard.component';
import { RadiusSearchComponent } from './radius-search/radius-search.component';
import { RadiusSearchDetailComponent } from './radius-search/radius-search-detail/radius-search-detail.component';
import { RouteSearchComponent } from './route-search/route-search.component';
import { RouteSearchDetailComponent } from './route-search/route-search-detail/route-search-detail.component';
import { NewComponent } from './route-search/new/new.component';
import { NewRadiusComponent } from './radius-search/new-radius/new-radius.component';
import { EditComponent } from './route-search/edit/edit.component';
import { RadiusEditComponent } from './radius-search/radius-edit/radius-edit.component';

const routes: Routes = [
  { path: 'home', component: AppComponent },
  { path: '', component: LandingpageComponent },
  { path: 'zoeken', component: SearchComponent },
  { path: 'dashboard', component: DashboardComponent },
  { path: 'masterdash', component: MasterDashboardComponent },
  { path: 'route/edit/:name', component: EditComponent },
  { path: 'zoeken/radius', component: RadiusSearchComponent },
  { path: 'radius/edit/:name', component: RadiusEditComponent },
  { path: 'zoeken/radius/new/:autocmpltInput/:radius', component: NewRadiusComponent },
  { path: 'zoeken/radius/detail/:type/:gipodId', component: RadiusSearchDetailComponent },
  { path: 'zoeken/route', component: RouteSearchComponent },
  { path: 'zoeken/route/new/:autocmpltOrigin/:autocmpltDest/:selectedTravelMode', component: NewComponent },
  { path: 'zoeken/route/detail/:type/:gipodId', component: RouteSearchDetailComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
