import { Component, OnInit } from '@angular/core';
import { AngularFireAuth } from 'angularfire2/auth';
import { AuthService } from '../../core/auth.service';

@Component({
  selector: 'app-navigation',
  templateUrl: './navigation.component.html',
  styleUrls: ['./navigation.component.scss'],
  providers: [AuthService]
})
export class NavigationComponent implements OnInit {

  constructor(public afAuth: AngularFireAuth, public authService: AuthService) {
    this.afAuth.authState.subscribe(user => {
      if (user) {
        console.log('user', user);
      }
    });
   }

  ngOnInit() {
  }

}
