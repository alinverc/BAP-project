import { Component, OnInit } from '@angular/core';
declare var google: any;
import { AngularFireModule } from 'angularfire2';
import { AngularFireDatabase, FirebaseListObservable } from 'angularfire2/database-deprecated';
import { AngularFireAuth } from 'angularfire2/auth';
import { Observable } from 'rxjs/Observable';
import * as firebase from 'firebase';
import { AuthService } from '../core/auth.service';
import { RouteService } from '../providers/route.service';
import { RadiusService } from '../providers/radius.service';
import { FormsModule, ReactiveFormsModule, FormGroup, FormBuilder, Validators } from '@angular/forms';
import { FirebaseApp } from 'angularfire2/firebase.app.module';
import { NavigationComponent } from '../core/navigation/navigation.component';
import { Route } from '../models/route';
import { RouteSearchService } from '../providers/route-search.service';
import { RadiusSearchService } from '../providers/radius-search.service';
import { Manifestation } from '../models/manifestation';
import { Workassignment } from '../models/workassignment';
import { ManifestationService } from '../providers/manifestation.service';
import { WorkassignmentService } from '../providers/workassignment.service';
import { Detail } from '../models/detail';
import { WorkassignmentDetail } from '../models/workassignment-detail';
import * as moment from 'moment';
import { styles } from '../styles/map';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss'],
  providers: [AuthService, RouteService, RouteSearchService,
    ManifestationService, WorkassignmentService, RadiusService, RadiusSearchService]
})
export class DashboardComponent implements OnInit {
  message: FirebaseListObservable<any[]>;
  addresses = [];
  disableBtn: boolean;
  email: string;
  password: string;
  userId: string;
  savedRadiusAddresses = [];
  routes: Observable<any[]>;
  radii: Observable<any[]>;
  markers = [];
  map: any;
  newMap: any;
  overviewPolyline: any;
  decodedPolyline: any;
  newPolyline: any;
  inputOrigin: any;
  inputDestination: any;
  adressInput: any;
  private manifestations: Manifestation[] = [];
  private workassignments: Workassignment[] = [];
  private error: string;
  workassignment: any;
  manifestation: any;
  info = [];
  latLng = [];
  positions = [];
  details: Detail[] = [];
  manifestationDescs = [];
  private workassignmentRadius: Workassignment[] = [];
  private manifestationsRadius: Manifestation[] = [];

  constructor(public authService: AuthService,
    public afAuth: AngularFireAuth,
    public af: AngularFireDatabase,
    public routeService: RouteService,
    public radiusService: RadiusService,
    private firebaseAuth: AngularFireAuth,
    private db: AngularFireDatabase,
    private routeSearchService: RouteSearchService,
    private radiusSearchService: RadiusSearchService,
    private manifestationService: ManifestationService,
    private workassignmentService: WorkassignmentService) {
      this.afAuth.authState.subscribe(user => {
        if (user) {
          this.userId = user.uid;
          this.message = this.af.list('/messages/' + this.userId);
        }
      });
     }

  ngOnInit() {
  // render map
    const location = {lat: 51.0543, lng: 3.7174};
    this.map = new google.maps.Map(document.getElementById('map'), {
      center: location,
      zoom: 13,
      styles: styles,
      zoomControl: false,
      mapTypeControl: false,
      scaleControl: false,
      streetViewControl: false,
      fullscreenControl: false,
    });
    this.afAuth.authState.subscribe(res => {
      if (res && res.uid) {
        console.log('user is logged in');
      } else {
        console.log('user not logged in');
      }
    });
    this.afAuth.authState.subscribe(user => {
      if (user) {
        this.userId = user.uid;
        this.routes = this.routeService.getRoutesList(this.userId);
        // get user radii
        this.radii = this.radiusService.getRadiiList();
        console.log('user', this.userId, this.radii);
        // get address of each radius and geocode
        let geocoder;
        geocoder = new google.maps.Geocoder();
        const self = this;
        this.radii.subscribe(
          thingies => {
            thingies.map(radius =>
              geocoder.geocode( { 'address': radius.address}, function(results) {
                self.savedRadiusAddresses.push({
                  address: results[0].geometry.location,
                });
              })
              );
          });
      }
    });
    console.log('markers addresses:', this.savedRadiusAddresses, 'length', this.savedRadiusAddresses.length);
    this.savedRadiusAddresses.forEach(function (arrayElem){
      const markers = new google.maps.Marker({
          position: arrayElem.address,
          animation: google.maps.Animation.DROP, // BOUNCE
          map: this.map,
        });
        console.log('pls');
    });
     // get manifestations in Gent
     this.manifestationService
     .getmanifestations()
     .subscribe((manifestations: Manifestation[]) => {
       this.manifestations = manifestations;
       // get coordinates from each manifestation with important hindrance, put in array
       const coordinates = this;
       let i = 0;
       for (i = 0; i < this.manifestations.length; i++) {
         if (this.manifestations[i].importantHindrance === false) {
           console.log('hindrance manifs', this.manifestations[i].importantHindrance);
          } else {
           coordinates.latLng.push({
             lat: this.manifestations[i].coordinate.coordinates[1],
             lng: this.manifestations[i].coordinate.coordinates[0],
             desc: this.manifestations[i].description,
             gipodId: this.manifestations[i].gipodId,
             type: 'manifestation'
            });
          }
       }
     });

     // get workassignments in Gent
     this.workassignmentService
     .getWorkassignments()
     .subscribe((workassignments: Workassignment[]) => {
       this.workassignments = workassignments;
       // get coordinates from each work with important hindrance, put in array
       const coordinates = this;
       let i = 0;
       for (i = 0; i < this.workassignments.length; i++) {
         if (this.workassignments[i].importantHindrance === false) {
           console.log('hindrance works', this.workassignments[i].importantHindrance);
          } else {
           coordinates.latLng.push({
             lat: this.workassignments[i].coordinate.coordinates[1],
             lng: this.workassignments[i].coordinate.coordinates[0],
             desc: this.workassignments[i].description,
             gipodId: this.workassignments[i].gipodId,
             type: 'workassignment'
            });
          }
       }
     });
  }

  // get manifestation and work latlng and make google.latlng
  getPositions() {
    let i = 0;
    for (i = 0; i < this.latLng.length; i++) {
      const googleThingie = new google.maps.LatLng(this.latLng[i].lat,
        this.latLng[i].lng);
      this.positions.push(googleThingie);
    }
    console.log('positions', this.positions);
  }

  // take address and geocode to get Lat and Lng
  geocode(point, bool) {
    let geocoder;
    const self = this;
    geocoder = new google.maps.Geocoder();
    geocoder.geocode( { 'address': point}, function(results, status) {
      if (bool === true) {
        self.inputOrigin = results[0].geometry.location;
      } else if (bool === false) {
        self.inputDestination = results[0].geometry.location;
      }
      if (status === 'OK') {
        console.log('geocode OK');
      } else {
        console.log('Er is iets misgegaan, probeer opnieuw');
        location.reload();
      }
    });
  }

  // delete a saved route
  deleteRoute(route: any) {
    if (confirm('Route zoekopdracht verwijderen?')) {
      this.af.object('/routes/' + route.$key).remove();
    } else {
        // Do nothing!
    }
  }

  // delete a saved route
  deleteRadius(radius: any) {
    if (confirm('Raduis zoekopdracht verwijderen?')) {
      this.af.object('/radius/' + radius.$key).remove();
    } else {
        // Do nothing!
    }
  }

  searchNowRoute(route: any) {
    this.info.length = 0;
    this.details.length = 0;
    const origin = route.origin;
    const destination = route.destination;
    const travelMode = route.travelMode;
    console.log(origin);
    // geocode inputs
    this.geocode(origin, true);
    this.geocode(destination, false);
    setTimeout(() => {
      console.log('origin', this.inputOrigin);
      console.log('destination', this.inputDestination);
    }, 500);
    setTimeout(() => {
      const self = this;
      this.routeSearchService.showRoute(self, this.inputOrigin, this.inputDestination, travelMode);
    }, 500);
    this.getPositions();
    setTimeout(() => {
      const self = this;
      console.log('new polyline', this.newPolyline, this.map);
      this.routeSearchService.searchOnPolyline(self, this.latLng, this.positions);
    }, 500);
    console.log('end', this.info);
    this.getNotificationDisplayInfo(this.info);
    // disable button for 1 sec to prevent errors
    this.disable();
    setTimeout(() => {
      this.disable();
    }, 1000);
  }

  getNotificationDisplayInfo (info) {
    this.details.length = 0;
    setTimeout(() => {
      let i = 0;
      for (i = 0; i < info.length; i++) {
        if (info[i].type === 'workassignment') {
          this.workassignmentService.GetWorkassignmentDetail(info[i].gipodId)
            .subscribe((workassignment) => {
              this.workassignment = workassignment;
              console.log(this.workassignment);
              this.details.push({
                gipodId: this.workassignment.gipodId,
                description: this.workassignment.description,
                location: {
                    coordinate: {
                        coordinates: this.workassignment.location.coordinate.coordinates
                    }
                },
                hindrance: {
                  effects: this.workassignment.hindrance.effects,
                  locations: this.workassignment.hindrance.locations,
                  streets: this.workassignment.hindrance.streets
              }
              });
            });
        } else {
          this.manifestationService.GetManifestationDetail(info[i].gipodId)
            .subscribe((manifestation) => {
              console.log(manifestation);
              this.manifestation = manifestation;
              this.details.push({
                gipodId: this.manifestation.gipodId,
                description: this.manifestation.description,
                location: {
                    coordinate: {
                        coordinates: this.manifestation.location.coordinate.coordinates
                    }
                },
                hindrance: {
                  effects: this.manifestation.hindrance.effects,
                  locations: this.manifestation.hindrance.locations,
                  streets: this.manifestation.hindrance.streets
              }
              });
            });
        }
      }
      console.log('details', this.details);
    }, 500);
  }

  searchNowRadius(radius: any) {
    this.info.length = 0;
    this.workassignmentRadius.length = 0;
    this.manifestationsRadius.length = 0;

    const address = radius.address;
    const radiusInput = radius.radius;
    const type = 'manifestation';
    const self = this;
    this.radiusSearchService.geocode(self, address);

    setTimeout(() => {
      console.log('addresinput', this.adressInput);
      const lat = this.adressInput.lat();
      const lng = this.adressInput.lng();

      // WORKASSIGNMENTS
      this.workassignmentService.
      GetWorkassignmentsRadius(lng, lat, radiusInput)
      .subscribe((workassignments: Workassignment[]) => {
        console.log(workassignments, 'radius search workassignments');
        this.workassignmentRadius = workassignments;
        // check if workassignments are found
        if (workassignments.length === 0) {
          console.log('no workassignments found');
          return;
        }
        // get coordinates and descriptions of found workassignments
        this.radiusSearchService.getCoordsDescs(self, this.workassignmentRadius, 'workassignment');
      }); // works stuff

      // MANIFESTATIONS
      this.manifestationService
      .GetManifestationsRadius(lng, lat, radiusInput)
      .subscribe((manifestations: Manifestation[]) => {
        console.log(manifestations, 'radius search manifestations');
        this.manifestationsRadius = manifestations;
        // check if any manifestations are found
        if (manifestations.length === 0) {
          this.error = 'Er zijn geen werken gevonden!';
        }
        // get coordinates and descriptions of found manifestations
        this.radiusSearchService.getCoordsDescs(self, this.manifestationsRadius, 'manifestation');
      });
      console.log(this.info, 'descs');
      this.getNotificationDisplayInfo(this.info);
    }, 500);
    // display markers
    setTimeout(() => {
      this.map = new google.maps.Map(document.getElementById('map'), {
        center: this.adressInput,
        zoom: 13,
        styles: styles,
        zoomControl: false,
        mapTypeControl: false,
        scaleControl: false,
        streetViewControl: false,
        fullscreenControl: false,
      });
      let i = 0;
      for (i = 0; i < this.markers.length; i++) {
        const markers = new google.maps.Marker({
          position: this.markers[i],
          animation: google.maps.Animation.DROP, // BOUNCE
          map: this.map,
        });
      }
      const radiusCircle = new google.maps.Circle({
        strokeColor: '#5FA385',
        strokeOpacity: 0.8,
        strokeWeight: 2,
        fillColor: '#A6D395',
        fillOpacity: 0.35,
        map: this.map,
        center: this.adressInput,
        radius: +radiusInput
      });
      const marker = new google.maps.Marker({
        position: this.adressInput,
        animation: google.maps.Animation.BOUNCE, // BOUNCE
        map: this.map,
      });
    }, 1000);
     // disable button for 1 sec to prevent errors
     this.disable();
     setTimeout(() => {
       this.disable();
     }, 1000);
  }

  dismiss(i) {
    this.details.splice(i, 1);
  }

  disable() {
    this.disableBtn = !this.disableBtn;
  }

}
