import { Component, OnInit } from '@angular/core';
declare var google: any;
import { AngularFireModule } from 'angularfire2';
import { AngularFireDatabase, FirebaseListObservable } from 'angularfire2/database-deprecated';
import { AngularFireAuth } from 'angularfire2/auth';
import { Observable } from 'rxjs/Observable';
import * as firebase from 'firebase';
import { AuthService } from '../../core/auth.service';
import { RouteService } from '../../providers/route.service';
import { RadiusService } from '../../providers/radius.service';
import { FormsModule, ReactiveFormsModule, FormGroup, FormBuilder, Validators } from '@angular/forms';
import { FirebaseApp } from 'angularfire2/firebase.app.module';
import { NavigationComponent } from '../../core/navigation/navigation.component';
import { Route } from '../../models/route';
import { RouteSearchService } from '../../providers/route-search.service';
import { RadiusSearchService } from '../../providers/radius-search.service';
import { Manifestation } from '../../models/manifestation';
import { Workassignment } from '../../models/workassignment';
import { ManifestationService } from '../../providers/manifestation.service';
import { WorkassignmentService } from '../../providers/workassignment.service';
import { Detail } from '../../models/detail';
import { WorkassignmentDetail } from '../../models/workassignment-detail';
import * as moment from 'moment';
import { styles } from '../../styles/map';
import { Router } from '@angular/router';

@Component({
  selector: 'app-master-dashboard',
  templateUrl: './master-dashboard.component.html',
  styleUrls: ['./master-dashboard.component.scss'],
  providers: [AuthService, RouteService, RouteSearchService,
    ManifestationService, WorkassignmentService, RadiusService, RadiusSearchService]
})
export class MasterDashboardComponent implements OnInit {
  message: FirebaseListObservable<any[]>;
  usersId;
  users;
  notifBody = [];
  addresses = [];
  disableBtn: boolean;
  email: string;
  password: string;
  userId: string;
  savedRadiusAddresses = [];
  routes: Observable<any[]>;
  route: Observable<any[]>;
  radii: Observable<any[]>;
  markers = [];
  map: any;
  newMap: any;
  overviewPolyline: any;
  decodedPolyline: any;
  newPolyline: any;
  inputOrigin: any;
  inputDestination: any;
  adressInput: any;
  private manifestations: Manifestation[] = [];
  private workassignments: Workassignment[] = [];
  private error: string;
  workassignment: any;
  manifestation: any;
  info = [];
  latLng = [];
  positions = [];
  details = [];
  manifestationDescs = [];
  private workassignmentRadius: Workassignment[] = [];
  private manifestationsRadius: Manifestation[] = [];

  constructor(public authService: AuthService,
    public afAuth: AngularFireAuth,
    public af: AngularFireDatabase,
    public routeService: RouteService,
    public radiusService: RadiusService,
    private firebaseAuth: AngularFireAuth,
    private db: AngularFireDatabase,
    private routeSearchService: RouteSearchService,
    private radiusSearchService: RadiusSearchService,
    private manifestationService: ManifestationService,
    private workassignmentService: WorkassignmentService,
    private router: Router) {
      this.afAuth.authState.subscribe(user => {
        if (user.email === 'vercruysse.aline@gmail.com') {
          console.log('Master user logged in');
        } else {
          this.router.navigate(['/dashboard']);
        }
      });
      this.users = af.list('/users', { });
      console.log('all users', this.users);

     }

  ngOnInit() {
    const location = {lat: 51.0543, lng: 3.7174};
    this.map = new google.maps.Map(document.getElementById('map'), {
      center: location,
      zoom: 13,
      styles: styles,
      zoomControl: false,
      mapTypeControl: false,
      scaleControl: false,
      streetViewControl: false,
      fullscreenControl: false,
    });
    // get manifestations in Gent
    this.manifestationService
    .getmanifestations()
    .subscribe((manifestations: Manifestation[]) => {
      this.manifestations = manifestations;
      // get coordinates from each manifestation with important hindrance, put in array
      const coordinates = this;
      let i = 0;
      for (i = 0; i < this.manifestations.length; i++) {
        if (this.manifestations[i].importantHindrance === false) {
          console.log('hindrance manifs', this.manifestations[i].importantHindrance);
         } else {
          coordinates.latLng.push({
            lat: this.manifestations[i].coordinate.coordinates[1],
            lng: this.manifestations[i].coordinate.coordinates[0],
            desc: this.manifestations[i].description,
            gipodId: this.manifestations[i].gipodId,
            type: 'manifestation'
           });
         }
      }
    });

    // get workassignments in Gent
    this.workassignmentService
    .getWorkassignments()
    .subscribe((workassignments: Workassignment[]) => {
      this.workassignments = workassignments;
      // get coordinates from each work with important hindrance, put in array
      const coordinates = this;
      let i = 0;
      for (i = 0; i < this.workassignments.length; i++) {
        if (this.workassignments[i].importantHindrance === false) {
          console.log('hindrance works', this.workassignments[i].importantHindrance);
         } else {
          coordinates.latLng.push({
            lat: this.workassignments[i].coordinate.coordinates[1],
            lng: this.workassignments[i].coordinate.coordinates[0],
            desc: this.workassignments[i].description,
            gipodId: this.workassignments[i].gipodId,
            type: 'workassignment'
           });
         }
      }
    });

  }

  // get manifestation and work latlng and make google.latlng
  getPositions() {
    let i = 0;
    for (i = 0; i < this.latLng.length; i++) {
      const googleThingie = new google.maps.LatLng(this.latLng[i].lat,
        this.latLng[i].lng);
      this.positions.push(googleThingie);
    }
    console.log('positions', this.positions);
  }

  // take address and geocode to get Lat and Lng
  geocode(point, bool) {
    let geocoder;
    const self = this;
    geocoder = new google.maps.Geocoder();
    geocoder.geocode( { 'address': point}, function(results, status) {
      if (bool === true) {
        self.inputOrigin = results[0].geometry.location;
      } else if (bool === false) {
        self.inputDestination = results[0].geometry.location;
      }
      if (status === 'OK') {
        console.log('geocode OK');
      } else {
        console.log('Er is iets misgegaan, probeer opnieuw');
        location.reload();
      }
    });
  }

  getNotificationDisplayInfo (info, name) {
    this.details.length = 0;
    console.log('info', info);
    setTimeout(() => {
      let i = 0;
      for (i = 0; i < info.length; i++) {
        if (info[i].type === 'workassignment') {
          this.workassignmentService.GetWorkassignmentDetail(info[i].gipodId)
            .subscribe((workassignment) => {
              this.workassignment = workassignment;
              console.log(this.workassignment);
              this.details.push({
                searchName: name,
                gipodId: this.workassignment.gipodId,
                description: this.workassignment.description,
                location: {
                    coordinate: {
                        coordinates: this.workassignment.location.coordinate.coordinates
                    }
                },
                hindrance: {
                  effects: this.workassignment.hindrance.effects,
                  locations: this.workassignment.hindrance.locations,
                  streets: this.workassignment.hindrance.streets
              }
              });
            });
        } else {
          this.manifestationService.GetManifestationDetail(info[i].gipodId)
            .subscribe((manifestation) => {
              console.log(manifestation);
              this.manifestation = manifestation;
              this.details.push({
                searchName: name,
                gipodId: this.manifestation.gipodId,
                description: this.manifestation.description,
                location: {
                    coordinate: {
                        coordinates: this.manifestation.location.coordinate.coordinates
                    }
                },
                hindrance: {
                  effects: this.manifestation.hindrance.effects,
                  locations: this.manifestation.hindrance.locations,
                  streets: this.manifestation.hindrance.streets
              }
              });
            });
        }
      }
      console.log('details', this.details);
    }, 500);
  }

  disable() {
    this.disableBtn = !this.disableBtn;
  }

  searchNowRoute() {
    this.notifBody.length = 0;
    this.details.length = 0;
    this.info.length = 0;
    this.workassignmentRadius.length = 0;
    this.manifestationsRadius.length = 0;
    this.users.forEach(user => {
      user.forEach(u => {
        this.notifBody.length = 0;
        this.route = this.routeService.getRoutesList(u.userId);
        console.log('loopception', u, this.route);
        this.route.forEach(route => {
          route.forEach(r => {
            console.log('r', r);
            const origin = r.origin;
            const destination = r.destination;
            const travelMode = r.travelMode;
            const routeName = r.name;

            this.geocode(origin, true);
            this.geocode(destination, false);

            setTimeout(() => {
              console.log('origin', this.inputOrigin);
              console.log('destination', this.inputDestination);
            }, 500);

            setTimeout(() => {
              const self = this;
              this.routeSearchService.showRoute(self, this.inputOrigin, this.inputDestination, travelMode);
            }, 500);

            this.getPositions();
            setTimeout(() => {
              const self = this;
              console.log('new polyline', this.newPolyline, this.map);
              this.routeSearchService.searchOnPolyline(self, this.latLng, this.positions);
            }, 500);
            console.log('end', this.info);
            this.getNotificationDisplayInfo(this.info, routeName);
            // disable button for 1 sec to prevent errors
            this.disable();
            setTimeout(() => {
              this.disable();
            }, 1000);
          });
        });
        setTimeout(() => {
          let e = 0;
          for (e = 0; e < this.details.length; e++) {
            this.notifBody.push({
              hinder: this.details[e].description,
              route: this.details[e].searchName
            });
          }
          console.log('notif', this.notifBody);
          this.message = this.af.list('/messages/' + u.userId);
            this.message.push({
              title: 'Routes',
              body: this.notifBody
            });
        }, 1000);
      });
    });

   }
}
