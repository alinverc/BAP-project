import { Component, OnInit } from '@angular/core';
import { AngularFireModule } from 'angularfire2';
import { AngularFireDatabase, AngularFireList } from 'angularfire2/database';
import { AngularFireAuth } from 'angularfire2/auth';
import { Observable } from 'rxjs/Observable';
import * as firebase from 'firebase';
import { AuthService } from '../core/auth.service';
import { FormsModule, ReactiveFormsModule, FormGroup, FormBuilder, Validators } from '@angular/forms';
import { FirebaseApp } from 'angularfire2/firebase.app.module';
import { Router } from '@angular/router';
import { MessagingService } from '../providers/messaging.service';

@Component({
  selector: 'app-landingpage',
  templateUrl: './landingpage.component.html',
  styleUrls: ['./landingpage.component.scss'],
  providers: [AuthService, MessagingService, AngularFireDatabase]
})
export class LandingpageComponent implements OnInit {
  email: string;
  password: string;
  displayName: string;
  message;
  userId: string;
  currentUser;
  registerForm: boolean;
  loginForm: boolean;
  loggedIn: boolean;
  users;
  errorMsg;

  ngOnInit() {
    this.msgService.getPermission();
    this.msgService.receiveMessage();
    this.message = this.msgService.currentMessage;
  }

  constructor(public authService: AuthService,
    public afAuth: AngularFireAuth, private router: Router,
    private msgService: MessagingService,
    public af: AngularFireDatabase) {
    // check if user is logged in or not
    this.afAuth.authState.subscribe(res => {
      if (res && res.uid) {
        console.log('user is logged in');
        this.loggedIn = true;
      } else {
        console.log('user not logged in');
        this.loggedIn = false;
      }
    });
    this.registerForm = false;
    this.loginForm = true;
    this.users = af.list('/users');
  }

  showRegister() {
    this.registerForm = true;
    this.loginForm = false;
  }

  showLogin() {
    this.registerForm = false;
    this.loginForm = true;
  }

  signup() {
    this.authService.signup(this.email, this.password, this.displayName);
    this.afAuth.authState.subscribe(user => {
      if (user) {
        this.userId = user.uid;
        this.users.push({ email: this.email, userId: this.userId, username: this.displayName});
      }
    });
    // this.email = this.password = '';
  }

  login() {
    this.authService.login(this.email, this.password);
    this.email = this.password = '';
    this.afAuth.authState.subscribe(user => {
      if (user) {
        this.router.navigate(['dashboard']);
      } else {
        this.errorMsg = 'Er is iets misgegaan, probeer opnieuw';
        console.log(this.errorMsg);
      }
    });

  }

  logout() {
    this.authService.logout();
  }

}
