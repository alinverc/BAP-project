export class Route {
    origin?: any;
    destination?: any;
    userId?: string;
    name?: string;
    desc?: string;
}
