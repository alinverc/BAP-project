export class WorkassignmentDetail {
    gipodId?: number;
    description?: string;
    location?: {
        coordinate?: {
            coordinates?: [{}]
        };
    };
    hindrance?: {
        effects?: [{}],
        locations?: [{}],
        streets?: [{}]
    };
}
