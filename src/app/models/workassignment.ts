export class Workassignment {
    gipodId?: number;
    description?: string;
    detail?: string;
    cities?: string;
    coordinate?: {
        coordinates?: [{}]
    };
    importantHindrance?: boolean;
}
