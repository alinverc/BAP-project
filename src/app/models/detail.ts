export class Detail {
    gipodId?: number;
    description?: string;
    location?: {
        coordinate?: {
            coordinates?: [{}]
        };
    };
    hindrance?: {
        effects?: [{}],
        locations?: [{}],
        streets?: [{}]
    };
}
