export class Manifestation {
    gipodId?: number;
    description?: string;
    detail?: string;
    cities?: string;
    coordinate?: {
        coordinates?: [{}]
    }; 
    importantHindrance?: boolean; 
}